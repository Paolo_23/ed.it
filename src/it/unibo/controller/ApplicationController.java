package it.unibo.controller;

import java.io.IOException;
import java.net.MalformedURLException;

import javax.swing.JFrame;

import it.unibo.view.EndView;
import it.unibo.view.GUIFactory;
import it.unibo.view.GUIFactoryImpl;
import it.unibo.view.LoadingView;
import it.unibo.view.MigView;
import it.unibo.view.SelectDirView;

/**
 * Control the entire flow of the application.
 */
public class ApplicationController {
	
	// View component creator.
	public static GUIFactory gui;
	// All the view of the application.
	@SuppressWarnings("unused")
	private MigView mv;
	@SuppressWarnings("unused")
	private LoadingView lv;
	@SuppressWarnings("unused")
	private SelectDirView sdv;
	@SuppressWarnings("unused")
	private PhotoManager pm;
	@SuppressWarnings("unused")
	private EndView ev;
	// Path of the folder selected by the user.
	public static String path;
	public static String editPath;
	// Frames.
	private JFrame mainframe;
	
	public ApplicationController() {
		ApplicationController.gui = new GUIFactoryImpl();
		mv = new MigView(this);
	}
	
	public void chooseDir(final JFrame mainf) {
		this.mainframe = mainf;
		sdv = new SelectDirView(this);
	}
	
	public void startLoading() {
		if(!ApplicationController.path.equals("")) {
			mainframe.setVisible(false);
			mainframe.dispose();
			try {
				this.lv = new LoadingView(this);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			try {
				Runtime.getRuntime().exec(ApplicationController.editPath + "/res/SendNudes.exe " + ApplicationController.path + "/ " 
						+ ApplicationController.path + "/ " + ApplicationController.editPath + "/res/");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	public void endLoading() {
		pm = new PhotoManager();
		ev = new EndView(this);
	}
	
	public void returnToMain() {
		mv = new MigView(this);
	}
	
}






