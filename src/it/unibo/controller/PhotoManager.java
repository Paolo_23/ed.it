package it.unibo.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.apache.commons.io.FileUtils;

public class PhotoManager {
	
	List<List<String>> lines = new ArrayList<>();
	Scanner inputStream;
	List<String> percorsi = new ArrayList<String>();
	Map<String, ArrayList<Double>> mappa = new HashMap<String, ArrayList<Double>>();
	Map<String, String> finale = new HashMap<String, String>();
	
	public PhotoManager() {
		savePhotos();
		checkPhotos();
		createFolder();
	}

	private void savePhotos() {
		
		String fileName= ApplicationController.path + "/SendNudes.csv";
		File file= new File(fileName);
		
		try{
			inputStream = new Scanner(file);
			while(inputStream.hasNext()){
				String line= inputStream.next();
				String[] values = line.split(",");
				lines.add(Arrays.asList(values));
			}
			inputStream.close();
		}catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		for(int i = 1; i < lines.size(); i++) {
			lines.get(i);
			ArrayList<Double> ar = new ArrayList<Double>();
			for(int j = 1; j < 5; j++) {
				ar.add(Double.parseDouble(lines.get(i).get(j)));
				mappa.put(lines.get(i).get(0), ar);
			}
		}
		for(int i = 1; i < lines.size(); i++) {
			lines.get(i);
			percorsi.add(lines.get(i).get(0));
		}
	}
	
	/**
	 * Check, for every photo, the csv value and associate the photo to a string that describe its type.
	 */
	private void checkPhotos() {
		for(String string : percorsi) {
			ArrayList<Double> ar = mappa.get(string);
			if(ar.get(3) >= 50) {
				if(ar.get(0) > ar.get(1)) {
					if(ar.get(0) > ar.get(2)) {
						finale.put(string, "Sopra");
					} else {
						finale.put(string, "Sotto");
					}
				} else {
					if(ar.get(1) > ar.get(2)) {
						finale.put(string, "Buona");
					} else {
						finale.put(string, "Sotto");
					}
				}
			} else {
				finale.put(string, "Mossa");
			}

		} 
	}

	/**
	 *  Create a directory for every type of photo, if doesn't exists already.
	 */
	private void createFolder() {
		
		File sotto = new File(ApplicationController.path + "/Sotto_esposte");
		if(!sotto.exists()) {
			new File(ApplicationController.path + "/Sotto_esposte").mkdirs();
		}
		File sopra = new File(ApplicationController.path + "/Sovra_esposte");
		if(!sopra.exists()) {
			new File(ApplicationController.path + "/Sovra_esposte").mkdirs();
		}
		File buone = new File(ApplicationController.path + "/Buone");
		if(!buone.exists()) {
			new File(ApplicationController.path + "/Buone").mkdirs();
		}
		File mosse = new File(ApplicationController.path + "/Mosse");
		if(!mosse.exists()) {
			new File(ApplicationController.path + "/Mosse").mkdirs();
		}
		
		
		for(String string : percorsi) {
			if(finale.get(string).equals("Mossa")) {
				try {
					FileUtils.copyFileToDirectory(new File(ApplicationController.path + "/" + string), new File(ApplicationController.path + "/Mosse/"));
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				if(finale.get(string).equals("Buona")) {
					try {
						FileUtils.copyFileToDirectory(new File(ApplicationController.path + "/" + string), new File(ApplicationController.path + "/Buone/"));
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				if(finale.get(string).equals("Sotto")) {
					try {
						FileUtils.copyFileToDirectory(new File(ApplicationController.path + "/" + string), new File(ApplicationController.path + "/Sotto_esposte/"));
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				if(finale.get(string).equals("Sopra")) {
					try {
						FileUtils.copyFileToDirectory(new File(ApplicationController.path + "/" + string), new File(ApplicationController.path + "/Sovra_esposte/"));
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
//		try {
//			FileUtils.forceDelete(new File(ApplicationController.path + "\\SendNudes.csv"));
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
	}


}

