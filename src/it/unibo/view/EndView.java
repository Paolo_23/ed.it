package it.unibo.view;

import java.awt.GridLayout;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import it.unibo.controller.ApplicationController;
import it.unibo.utils.BackgroundPanel;
import it.unibo.utils.ImageLoaderUtils;
import net.miginfocom.swing.MigLayout;

public class EndView {
	
	private ApplicationController ac;
	private ImageLoaderUtils il;
	private String imagepath = "/loading.jpg";
	private String logopath = "/logo.png";
	private JFrame frame;
	private JPanel panel;
	
	public EndView(ApplicationController appcontr) {
		this.ac = appcontr;
		this.il = new ImageLoaderUtils();
		createFrame();
	}
	
	private void createFrame() {
		frame = ApplicationController.gui.createFrame("ED.IT");
		frame.setLayout(new GridLayout(1, 2));
		frame.setIconImage(il.loadImage(logopath));
		createPanels();
		frame.add(panel);
		frame.setVisible(true);
	}
	
	private void createPanels() {
		panel = new BackgroundPanel(il.loadImage(imagepath));
		panel.setLayout(new MigLayout("align center, fill"));
		createComponents();
	}
	
	private void createComponents() {
		JButton b = ApplicationController.gui.createButton("Return to Main Menu");
		b.addActionListener(e -> {
			e.getSource();
			this.frame.setVisible(false);
			ac.returnToMain();
		});
	}
}
