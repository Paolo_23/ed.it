package it.unibo.view;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import it.unibo.utils.GameFont;
import it.unibo.utils.ScreenToolUtils;

public class GUIFactoryImpl implements GUIFactory {
	
	private static final double SCALE = ScreenToolUtils.SCALE;
	private final GameFont font = new GameFont();

	@Override
	public JFrame createFrame(String title) {
		final JFrame frame = new JFrame();
		frame.setSize(new Dimension((int) (1100 * SCALE), (int) (650 * SCALE)));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocation((int) (80 * SCALE),(int) (50 * SCALE));
		frame.setResizable(false);
		return frame;
	}

	@Override
	public JButton createButton(String text) {
		final JButton button = new JButton(text);
		button.setFont(font.getFont());
		button.setBackground(Color.BLACK);
		button.setForeground(Color.WHITE);
		button.setFocusable(false);
        button.setBorderPainted(false);
        button.setFocusPainted(false);
		return button;
	}
	
	public JDialog createDialog() {
		JDialog dialog = new JDialog();
		dialog.setTitle("Choose dir");
		dialog.setSize(new Dimension((int) (400 * SCALE),(int) (200 * SCALE)));
		dialog.setLocation((int) (300 * SCALE),(int) (300 * SCALE));
		dialog.setVisible(true);
		return dialog;
	}

}